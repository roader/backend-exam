<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\CommentsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// for Guest account, but auth is currently mocked up
Route::get('/posts', [PostsController::class, 'index'])->name('posts.index');
Route::get('/posts/{post}', [PostsController::class, 'show'])->name('posts.show');

Route::get('/posts/{id}', [CommentsController::class, 'show'])->name('posts.show');
Route::get('/posts/{post}/comments', [CommentsController::class, 'index'])->name('comments.index');

// This is an Auth mockup (in a real scenario we need to use Sanctum or Passport, but this will work for testing) due to the latest Laravel 8 uses a different authentication than previous version (auth:api no longer on the latest documentation)
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
Route::post('/register', [AuthController::class, 'register'])->name('register');

Route::post('/posts', [PostsController::class, 'store'])->name('posts.create');
Route::delete('/posts/{id}', [PostsController::class, 'destroy'])->name('posts.delete');

Route::post('/posts/{post}/comments', [CommentsController::class, 'store'])->name('comments.create');
Route::patch('/posts/{post}', [CommentsController::class, 'update'])->name('posts.edit');
Route::delete('/posts/{post}/comments/{comment}', [CommentsController::class, 'destroy'])->name('posts.delete');
