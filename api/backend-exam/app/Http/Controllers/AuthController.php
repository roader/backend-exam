<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class AuthController extends Controller
{
    /**
     * Login
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        return response()->json([
            'success'=> 'true',
            'token' => '1234' // mock login due to latest Laravel 8 uses a different way of authenticating than previous version (will be needing Laravel Sanctum)
        ], Response::HTTP_OK);
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\Response
     */
    public function logut()
    {
        // Auth mockup

        return response()->json([
            'success'=> 'Logged out'
        ], Response::HTTP_OK);
    }

    /**
     * Register
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validationRules = [
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => 'min:8|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:8'
        ];

        $validator = Validator::make(request()->only(array_keys($validationRules)), $validationRules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' =>  'The given data was invalid.',
                'errors' => $validator->errors()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response()->json([
            'name' => $request['name'],
            'email' => $request['email'],
            'updated_at' => now(),
            'created_at' => now(),
            'id' => 1
        ], Response::HTTP_CREATED);
    }
}
