<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Symfony\Component\HttpFoundation\Response;

class PostsController extends Controller
{
    const perPage = 5;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'success'=> 'true',
            'data' => [Post::paginate(self::perPage)]
        ], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validationRules = [
            'title' => ['required'],
            'content' => ['required']
        ];

        $validator = Validator::make($request->only(array_keys($validationRules)), $validationRules);

        if ($validator->fails()) {
            return response()->json([
                'message' =>  'Invalid data.',
                'errors' => $validator->errors()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $additionalData = [];
        $additionalData['user_id'] = Auth()->user()->id;
        $additionalData['slug'] = Str::slug($request['title'], '-');

        $post = Post::create(array_merge($validator->valid(), $additionalData));

        return response()->json([
            'data' => $post
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($post)
    {
        if ($post->count() > 0) {
            return response()->json([
              'data' =>  $post
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' =>  'No query results for model [App\\Post].'
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if ($post) {
            $post->delete();

            return response()->json([
                'status' =>  'record deleted successfully'
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' =>  'Invalid data.',
        ], Response::HTTP_NOT_FOUND);
    }
}
