<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        return response()->json([
            'success'=> 'true',
            'data' => $post->comments
        ], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Post $post)
    {
        $validationRules = [
            'body' => ['required'],
            'commentable_id' => ['required', 'numeric'],
            'parent_id' => ['required', 'numeric']
        ];

        $validator = Validator::make(request()->only(array_keys($validationRules)), $validationRules);

        if ($validator->fails()) {
            return response()->json([
                'message' =>  'The given data was invalid.',
                'errors' => $validator->errors()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($post === null) {
            return response()->json([
                'message' =>  '"No query results for model'
            ], Response::HTTP_NOT_FOUND);
        };

        $additionalData = [];
        $additionalData['commentable_type'] = 'App\\Post';
        $additionalData['creator_id'] = Auth()->user()->id;

        $comment = Comment::create(array_merge($validator->valid(), $additionalData));

        return response()->json([
            'data' =>  $comment
        ], Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post, Comment $comment)
    {
        if ($comment) {
            $comment->delete();

            return response()->json([
              'status' =>  'record deleted successfully'
          ], Response::HTTP_OK);
        }

        return response()->json([
            'success' => false,
            'message' =>  'Invalid data.',
        ], Response::HTTP_NOT_FOUND);
    }
}
