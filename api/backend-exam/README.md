## Backend Exam

Please note that the authentication is currently been mocked up due to the latest Laravel uses a new way of authentication and the frontend codes needs to be updated also. For installation:

1. git clone GIT-URL
2. composer install
3. create an empty database for this project
4. cp .env.example .env # add the credential from the previous created database
5. php artisan key:generate
6. php migrate
7. php artisan serve


